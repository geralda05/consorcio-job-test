# Consorcio Job Test
### Front-end project made by Gerald Leonel Alarcón

## Online Server

Available on `http://consorcio-job-test.herokuapp.com/`

## Framework info

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Basic information
### Route /search
Contiene información nutrida utilizando solamente los componentes y typescript desde el front-end. No tiene ninguna API rest de por medio. Hace una petición inicial trayendo toda la información de las farmacias y después hace el filtro rapidamente una vez que el usuario interactua con los botones disponibles en la vista.

### Route /search-api
Contiene información nutrida desde la API REST hecha en Node.js generada por mi y disponible en `https://bitbucket.org/geralda05/consorcio-test-backend/src/master/`. Es el modulo mas sencillo ya que solamente envia información de la comuna y el texto a buscar y la API se encarga de buscar los resultados, filtrarlos y retornarlos de forma que solo se liste lo que es devuelto en la petición HTTP.