import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})

/* SERVICIO GLOBAL CON FUNCIONES REUTILIZABLES */

export class MainService {

  constructor(private sanitizer: DomSanitizer) { }

  /* Peticion directa de comunas sin API REST de por medio */
  getComunas(data) {
    const promise = new Promise((resolve, reject) => {
      axios({
        method:'post',
        url:'https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones',
        headers:{
          'Content-Type':'multipart/form-data'
        },
        data:data,
      }).then(response =>{
        resolve(this.sanitizer.bypassSecurityTrustHtml(response.data));
      }).catch(error => {
        reject(error)
      })
    });
    return promise;
  }

  /* Petición HTTP de farmacias a mi API REST utilizada en search-api */
  getFarmacias(data) {
    const promise = new Promise((resolve, reject) => {
      axios({
        method:'post',
        url:'http://consorcio-test-backend.herokuapp.com/farmacias',
        data:data,
      }).then(response =>{
        resolve(response);
      }).catch(error => {
        reject(error)
      })
    });
    return promise;
  }
}
