import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import axios from 'axios';
import * as moment from 'moment-timezone';
import {MainService} from '../../services/main-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {

  constructor(private sanitizer: DomSanitizer, public mainService: MainService) { }

  /* Definición de variables necesarias */
  public farmacias = [];
  public p = 1;
  content;
  public pageNumbers = 3;
  public loading = false;
  public comuna = '0';
  public name = '';
  public comunaSearch = '';
  public search = '';
  public filterFarmacias = [];
  public nameSearch = '';

  /* Cantidad de tarjetas por pagina dinamicamente */
  setPageNumber(v){
    this.pageNumbers = v;
  }
  
  public searched = false;
  /* Funcion ejecutada al realizar la consulta */
  consultar(){
    this.comunaSearch = this.comuna;
    this.name = this.search;
    /* Realizar el filtro solicitado por el usuario lo mas optimo posible */
    this.filterFarmacias = this.farmacias.filter(x => x.fk_comuna === this.comunaSearch).filter(x => (x.local_nombre.toLowerCase().includes(this.nameSearch.toLowerCase())));
    this.searched = true;
  }

  public errorComunas = false;

  readFarmacias(){
    /* Peticion HTTP para leer farmacias disponibles */
    axios({
      method:'get',
      url:'https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion',
      params:{
        id_region:7
      },
      headers:{
        'Content-Type':'application/json'
      }
    }).then(response =>{
      /* Formatear datos recibidos */
      this.farmacias = response.data.map(x => {

        let current = x;
        current.local_lng = Number(x.local_lng);
        current.local_lat = Number(x.local_lat);

        /* Función para determinar si la farmacia está abierta al momento de realizar la consulta (función hecha en la API REST de la otra vista) */
          const hora_separada = x.funcionamiento_hora_apertura.split(' ')[0].split(':');
          const hora_cierre = x.funcionamiento_hora_cierre.split(' ')[0].split(':');
          const apertura = moment().tz('America/Santiago').set({hour: hora_separada[0],minute:hora_separada[1],second:0,millisecond:0}); 
          const cierre = moment().tz('America/Santiago').set({hour: hora_cierre[0],minute:hora_cierre[1],second:0,millisecond:0}); 
          const actual = moment().tz('America/Santiago');
          if(apertura.valueOf() <= actual.valueOf() && actual.valueOf() < cierre.valueOf()){
            current.open = 'Abierto'
          }else{
            current.open = 'Cerrado'
          }

        return current
      });
    }).catch(error =>{
      /* Manejo de errores */
      Swal.fire({
        title: 'Ocurrió un error al buscar las farmacias',
        text: "¿Desea intentarlo nuevamente?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Buscar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          this.readFarmacias();
        }
      })
    })
  }

  ngOnInit(): void {
    this.readFarmacias();
    this.readComunas();
  }

  readComunas(){
    let bodyFormData = new FormData();
    bodyFormData.append('reg_id', '7'); 
    this.loading = true;
    this.mainService.getComunas(bodyFormData).then(response => {
      this.loading = false;
      this.content = response;
    }).catch(error => {
      this.errorComunas = true;
      Swal.fire({
        title: 'Ocurrió un error al buscar las comunas',
        text: "¿Desea intentarlo nuevamente?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Buscar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          this.readComunas();
        }
      })
    });
  }

}
