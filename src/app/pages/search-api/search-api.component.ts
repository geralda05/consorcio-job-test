import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import axios from 'axios';
import * as moment from 'moment-timezone';
import {MainService} from '../../services/main-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-search-api',
  templateUrl: './search-api.component.html',
  styleUrls: ['./search-api.component.scss']
})

export class SearchApiComponent implements OnInit {

  constructor(private sanitizer: DomSanitizer, public mainService: MainService) { }
  public p = 1;
  content;
  public pageNumbers = 3;
  public loading = false;
  public comuna = '0';
  public search = '';
  public filterFarmacias = [];
  public errorComunas = false;
  public loadingResults = false;
  public searched = false;

  setPageNumber(v){
    this.pageNumbers = v;
  }

  readFarmacias(){
    try{
      this.loadingResults = true;
      this.filterFarmacias = [];
      /* Leer farmacias desde la API */
      this.mainService.getFarmacias({
        "comuna":this.comuna,
        "nombre":this.search,
        "id_region":7
      }).then(response => {
        this.searched = true;
        this.filterFarmacias = response['data']['data'].map(x => {
          /* Formatear datos para usar el componente global reutilizado en ambas vistas */
          return {
            local_nombre: x.nombre,
            local_direccion: x.direccion,
            localidad_nombre: '',
            telefono: x.telefono,
            open: (x.abierta === true) ? 'Abierto':'Cerrado',
            local_lat: x.latitud,
            local_lng: x.longitud
          }
        })
      }).catch(error => {
        /* Manejo de errores */
        Swal.fire({
          title: 'Ocurrió un error al buscar las farmacias',
          text: "¿Desea intentarlo nuevamente?",
          icon: 'error',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Buscar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.isConfirmed) {
            this.readFarmacias();
          }
        })
      }).finally(() => {
        this.loadingResults = false;
      })
    }catch{
      Swal.fire({
        title: 'Error interno',
        icon: 'error',
      })
    }
  }

  ngOnInit(): void {
    this.readComunas();
  }

  readComunas(){
    let bodyFormData = new FormData();
    bodyFormData.append('reg_id', '7'); 
    this.loading = true;
    this.mainService.getComunas(bodyFormData).then(response => {
      this.loading = false;
      this.content = response;
    }).catch(error => {
      this.errorComunas = true;
      Swal.fire({
        title: 'Ocurrió un error al buscar las comunas',
        text: "¿Desea intentarlo nuevamente?",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Buscar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          this.readComunas();
        }
      })
    });
  }

}
