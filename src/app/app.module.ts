import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { SearchComponent } from './pages/search/search.component';
import { AgmCoreModule } from '@agm/core';
import { Pipe, PipeTransform } from '@angular/core'; 
import {FormsModule} from '@angular/forms';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { FarmaciaCardComponent } from './components/farmacia-card/farmacia-card.component';
import { SearchApiComponent } from './pages/search-api/search-api.component';
import { HeaderAppComponent } from './components/header-app/header-app.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    FarmaciaCardComponent,
    SearchApiComponent,
    HeaderAppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng2SearchPipeModule,
    FormsModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBEJAbF1hqCcVMYLkh08vAzH9G7Hujo-4Y'
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
