import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmaciaCardComponent } from './farmacia-card.component';

describe('FarmaciaCardComponent', () => {
  let component: FarmaciaCardComponent;
  let fixture: ComponentFixture<FarmaciaCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmaciaCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmaciaCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
