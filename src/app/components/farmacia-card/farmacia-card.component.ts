import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'farmacia-card',
  templateUrl: './farmacia-card.component.html',
  styleUrls: ['./farmacia-card.component.scss']
})

export class FarmaciaCardComponent implements OnInit {

  constructor() { }

  @Input() farmacia;

  ngOnInit(): void {
  }

  public icons = {
    'cruz verde':'https://www.isapredecodelco.cl/wp-content/uploads/2020/08/Cruz-Verde.png',
    'cruz verde ':'https://www.isapredecodelco.cl/wp-content/uploads/2020/08/Cruz-Verde.png',
    'salcobrand':'../../../assets/images/salcobrand.jpg',
    'salcobrand ':'../../../assets/images/salcobrand.jpg',
    'ahumada':'../../../assets/images/ahumada.jpg',
    'ahumada ':'../../../assets/images/ahumada.jpg',
  }
}
